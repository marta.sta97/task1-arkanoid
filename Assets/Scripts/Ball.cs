﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
	public bool isLightningBall;
	private SpriteRenderer spriteRenderer;
	private float lightningBallDuration = 5f;

	public ParticleSystem lightningEffect;

	public static event Action<Ball> OnBallDeath;
	public static event Action<Ball> OnLightningBallEnable;
	public static event Action<Ball> OnLightningBallDisable;

	private void Awake()
	{
		this.spriteRenderer = GetComponentInChildren<SpriteRenderer>();
	}

	public void Die()
	{
		OnBallDeath?.Invoke(this);
		Destroy(gameObject, 1f);
	}

	public void StartLightningBall()
	{
		if (!this.isLightningBall)
		{
			this.isLightningBall = true;
			this.spriteRenderer.enabled = false;

			lightningEffect.gameObject.SetActive(true);
			StartCoroutine(StopLightningBallAfterTime(this.lightningBallDuration));

			OnLightningBallEnable?.Invoke(this);
		}
	}

	private IEnumerator StopLightningBallAfterTime(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		StopLightningBall();
	}

	private void StopLightningBall()
	{
		if (this.isLightningBall)
		{
			this.isLightningBall = false;
			this.spriteRenderer.enabled = true;
			lightningEffect.gameObject.SetActive(false);

			OnLightningBallDisable?.Invoke(this);
		}
	}
}
