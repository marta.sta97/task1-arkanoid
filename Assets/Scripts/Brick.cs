﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static UnityEngine.ParticleSystem;

public class Brick : MonoBehaviour
{

	private SpriteRenderer spriteRenderer;
	private BoxCollider2D boxCollider;

	public int Hitpoints = 1;

	public ParticleSystem DestroyEffect;

	public static event Action<Brick> OnBrickDestruction;

	private void Awake()
	{
		this.spriteRenderer = this.GetComponent<SpriteRenderer>();
		this.boxCollider = this.GetComponent<BoxCollider2D>();
		Ball.OnLightningBallEnable += OnLightningBallEnable;
		Ball.OnLightningBallDisable += OnLightningBallDisable;
	}

	private void OnLightningBallEnable(Ball obj)
	{
		if (this != null)
		{
			this.boxCollider.isTrigger = true;
		}
	}

	private void OnLightningBallDisable(Ball obj)
	{
		if (this != null)
		{
			this.boxCollider.isTrigger = false;
		}
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		Ball ball = other.gameObject.GetComponent<Ball>();
		if (ball != null)
		{
			ApplyCollisionLogic(ball);
		}
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		Ball ball = other.gameObject.GetComponent<Ball>();
		if (ball != null)
		{
			ApplyCollisionLogic(ball);
		}
	}

	private void ApplyCollisionLogic(Ball ball)
	{
		this.Hitpoints--;

		if (this.Hitpoints <= 0 || ball.isLightningBall)
		{
			BricksManager.Instance.RemainingBricks.Remove(this);
			OnBrickDestruction?.Invoke(this);
			OnBrickDestroy();
			SpawnDestroyEffect();
			Destroy(this.gameObject);
		}
		else
		{
			this.spriteRenderer.sprite = BricksManager.Instance.Sprites[this.Hitpoints - 1];
		}
	}

	private void OnBrickDestroy()
	{
		float buffSpawnChance = UnityEngine.Random.Range(0, 100f);
		float debuffSpawnChance = UnityEngine.Random.Range(0, 100f);
		bool alreadySpawned = false;

		if (buffSpawnChance <= CollectablesManager.Instance.BuffChance)
		{
			alreadySpawned = true;
			Collectable newBuff = this.SpawnCollectable(true);
		}

		if (buffSpawnChance <= CollectablesManager.Instance.BuffChance && !alreadySpawned)
		{
			alreadySpawned = true;
			Collectable newBuff = this.SpawnCollectable(false);
		}
	}

	private Collectable SpawnCollectable(bool isBuff)
	{
		List<Collectable> collection;

		if (isBuff)
		{
			collection = CollectablesManager.Instance.AvailableBuffs;
		}
		else
		{
			collection = CollectablesManager.Instance.AvailableDebuffs;
		}

		int buffIndex = UnityEngine.Random.Range(0, collection.Count);
		Collectable prefab = collection[buffIndex];
		Collectable newCollectable = Instantiate(prefab, this.transform.position, Quaternion.identity) as Collectable;

		return newCollectable;
	}

	private void SpawnDestroyEffect()
	{
		Vector3 brickPosition = gameObject.transform.position;
		Vector3 spawnPosition = new Vector3(brickPosition.x, brickPosition.y, brickPosition.z - 0.2f);
		GameObject effect = Instantiate(DestroyEffect.gameObject, spawnPosition, Quaternion.identity);

		MainModule mm = effect.GetComponent<ParticleSystem>().main;
		mm.startColor = this.spriteRenderer.color;
		Destroy(effect, DestroyEffect.main.startLifetime.constant);
	}

	internal void Init(Transform containerTransform, Sprite sprite, Color color, int hitPoints)
	{
		this.transform.SetParent(containerTransform);
		this.spriteRenderer.sprite = sprite;
		this.spriteRenderer.color = color;
		this.Hitpoints = hitPoints;
	}

	private void OnDisable()
	{
		Ball.OnLightningBallEnable -= OnLightningBallEnable;
		Ball.OnLightningBallDisable -= OnLightningBallDisable;
	}
}
