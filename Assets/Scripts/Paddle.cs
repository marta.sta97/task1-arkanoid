﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paddle : MonoBehaviour
{

	#region Singleton

	private static Paddle _instance;

	public static Paddle Instance => _instance;

	public bool IsTransforming { get; set; }

	private void Awake()
	{
		if (_instance != null)
		{
			Destroy(gameObject);
		}
		else
		{
			_instance = this;
		}
	}

	#endregion

	private Camera mainCamera;

	private float paddleInitialY;
	private BoxCollider2D boxCollider;

	private float defaultPaddleWidthInPixels = 200;
	private float defaultLeftClamp = 135;
	private float defaultRightClamp = 410;

	private SpriteRenderer spriteRenderer;
	private float extendShrinkDuration = 10f;
	private float paddleWidth = 2f;
	private float paddleHeight = 0.28f;

	// Start is called before the first frame update
	void Start()
	{
		mainCamera = FindObjectOfType<Camera>();
		paddleInitialY = this.transform.position.y;
		spriteRenderer = GetComponent<SpriteRenderer>();
		boxCollider = GetComponent<BoxCollider2D>();
	}

	// Update is called once per frame
	void Update()
	{
		PaddleMovement();
	}

	public void StartWidthAnimation(float newWidth)
	{
		StartCoroutine(AnimatePaddleWidth(newWidth));
	}

	public IEnumerator AnimatePaddleWidth(float width)
	{
		this.IsTransforming = true;
		this.StartCoroutine(ResetPadleWidthAfterTime(this.extendShrinkDuration));

		if (width > this.spriteRenderer.size.x)
		{
			float currentWidth = this.spriteRenderer.size.x;
			while (currentWidth < width)
			{
				currentWidth += Time.deltaTime * 2;
				this.spriteRenderer.size = new Vector2(currentWidth, paddleHeight);
				boxCollider.size = new Vector2(currentWidth, paddleHeight);
				yield return null;
			}
		}
		else
		{
			float currentWidth = this.spriteRenderer.size.x;
			while (currentWidth > width)
			{
				currentWidth -= Time.deltaTime * 2;
				this.spriteRenderer.size = new Vector2(currentWidth, paddleHeight);
				boxCollider.size = new Vector2(currentWidth, paddleHeight);
				yield return null;
			}
		}

		this.IsTransforming = false;
	}

	private IEnumerator ResetPadleWidthAfterTime(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		this.StartWidthAnimation(this.paddleWidth);
	}

	private void PaddleMovement()
	{
		float paddleShift = (defaultPaddleWidthInPixels - ((defaultPaddleWidthInPixels * 0.5f) * this.spriteRenderer.size.x)) * 0.5f;
		float leftClamp = defaultLeftClamp - paddleShift;
		float rightClamp = defaultRightClamp + paddleShift;
		float mousePositionPixels = Mathf.Clamp(Input.mousePosition.x, leftClamp, rightClamp);
		float mousePositionWorldX = mainCamera.ScreenToWorldPoint(new Vector3(mousePositionPixels, 0, 0)).x;
		this.transform.position = new Vector3(mousePositionWorldX, paddleInitialY, 0);
	}


	private void OnCollisionEnter2D(Collision2D other)
	{
		if (other.gameObject.tag == "Ball")
		{
			Rigidbody2D ballRb = other.gameObject.GetComponent<Rigidbody2D>();
			Vector3 hitPoint = other.contacts[0].point;

			Vector3 paddlePosition = this.gameObject.transform.position;
			Vector3 paddleCenter = new Vector3(paddlePosition.x, paddlePosition.y);

			ballRb.velocity = Vector2.zero;

			float difference = paddleCenter.x - hitPoint.x;

			if (hitPoint.x < paddleCenter.x)
			{
				ballRb.AddForce(new Vector2(-(Mathf.Abs(difference * 200)), BallsManager.Instance.initialBallSpeed));
			}
			else
			{
				ballRb.AddForce(new Vector2((Mathf.Abs(difference * 200)), BallsManager.Instance.initialBallSpeed));
			}
		}
	}
}
