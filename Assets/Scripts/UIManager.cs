﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
	public Text TargetText;
	public Text LivesText;
	public Text ScoreText;

	public int Score { get; set; }

	private void Start()
	{
		UpdateLivesText(GameManager.Instance.AvailibleLives);
	}

	private void Awake()
	{
		Brick.OnBrickDestruction += OnBrickDestruction;
		BricksManager.OnLevelLoaded += OnLevelLoaded;
		GameManager.OnLifeLost += OnLifeLost;
	}

	private void OnLifeLost(int remainingLives)
	{
		UpdateLivesText(remainingLives);
	}

	private void UpdateLivesText(int remainingLives)
	{
		LivesText.text = $"LIVES: {remainingLives}";
	}

	private void OnLevelLoaded()
	{
		UpdateRemainingBricksText();
		UpdateScoreText(0);
	}

	private void UpdateScoreText(int increment)
	{
		this.Score += increment;
		string scoreString = this.Score.ToString().PadLeft(5, '0');
		ScoreText.text = $"SCORE:{Environment.NewLine}{scoreString}";
	}

	private void OnBrickDestruction(Brick obj)
	{
		UpdateRemainingBricksText();
		UpdateScoreText(10);
	}

	private void UpdateRemainingBricksText()
	{
		TargetText.text = $"TARGET:\n{BricksManager.Instance.RemainingBricks.Count}/{BricksManager.Instance.InitialBricksCount}";
	}

	private void OnDisable()
	{
		Brick.OnBrickDestruction -= OnBrickDestruction;
		BricksManager.OnLevelLoaded -= OnLevelLoaded;
		GameManager.OnLifeLost -= OnLifeLost;
	}
}
